class RecipeIngredient < ActiveRecord::Base
  belongs_to :recipe
  belongs_to :ingredient
  validates :ingredient, presence: true
  validates :quantity, presence: true
end
