class Recipe < ActiveRecord::Base
  #has_attached_file :image, styles: { medium: "400x400>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  #validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  has_many :recipe_ingredients
  has_many :ingredients, through: :recipe_ingredients
  has_many :comments
  belongs_to :user

  validates :title, presence: true
  validates :content, presence: true
  validates :recipe_ingredients, length: { minimum: 1 }

  accepts_nested_attributes_for :recipe_ingredients
end
