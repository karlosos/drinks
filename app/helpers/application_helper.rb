module ApplicationHelper
  def link_to_add_object(name, f, association, css_class, partial_prefix = '')
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(partial_prefix + association.to_s.singularize + '_fields', f: builder)
    end
    link_to(name, '#', class: css_class, data: { id: id, fields: fields.delete("\n") })
  end
end
