class AddAttachmentImageToRecipes < ActiveRecord::Migration
  change_table :recipes do |t|
    t.string :image
  end
end
